package org.piglatin.translator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * <p>PigLatin is a language game in which words in English are altered by specified rules.
 * For example: "Wikipedia" would become "Ikipediaway".<br>
 * Rules: <br>
 * 1. Words that start with a consonant have their first letter moved to
 * the end of the word and the letters “ay” added to the end.<br>
 * 2. Words that start with a vowel have the letters “way” added to the end.<br>
 * 3. Words that end in “way” are not modified.<br>
 * 4. Punctuation must remain in the same relative place from the end of the word.<br>
 * 5. Hyphens are treated as two words.<br>
 * 6. Capitalization must remain in the same place.
 * </p>
 *
 * @author Petr Novák (petr.novak@starkysclub.com)
 * @implNote {@link this#processWord(String)} does not process multiple consonant
 * characters, which would make vowel sound, as vowel (rule no. 2, e.g. "xr", "yt"). <br>
 * Similarly there is no impl for consonant clusters, whose pronunciation makes distinct consonant sound
 * and should all be moved to the back of the word (rule no. 1).
 * @since 01.07.2019
 */
public final class PigLatinUtil {

    private static final String VOWELS = "aeiouAEIOU";
    private static final String PUNCTUATION = ".,?!;[]{}()'*";

    private static final List<String> ENDING_AY = Arrays.asList("a", "y");
    private static final List<String> ENDING_WAY = Arrays.asList("w", "a", "y");

    private PigLatinUtil() {
    }

    public static String processSentence(String sentence) {
        String result = Arrays.stream(sentence.split(" "))
                .map(PigLatinUtil::processWord)
                .collect(Collectors.joining(" "));
        return result;
    }

    public static String processWord(String givenWord) {
        // RULE 5: word with hyphen
        if (givenWord.contains("-")) {
            String result = processHyphen(givenWord);
            return result;
        }

        // RULE 3: word with selected chars ending
        if (isWordEndingWith(givenWord, ENDING_WAY)) {
            return givenWord;
        }

        // get word as list of String characters
        List<String> characters = Arrays.asList(givenWord.split("(?!^)"));

        // RULE 4 & 6 begin
        Map<Integer, String> punctuations = getPunctuations(characters);
        List<Integer> capitalization = getCapitalizationIndexes(givenWord);

        // fix to lowercase
        givenWord = givenWord.toLowerCase();
        characters = new LinkedList<>(Arrays.asList(givenWord.split("(?!^)")));
        // remove punctuation
        characters.removeIf(PUNCTUATION::contains);

        // RULE 1 and 2 begin
        if (VOWELS.contains(characters.get(0))) {
            characters.addAll(ENDING_WAY);
        } else {
            String removed = characters.remove(0);
            characters.add(removed);
            characters.addAll(ENDING_AY);
        }

        // RULE 4: return back punctuation
        for (Map.Entry<Integer, String> entry : punctuations.entrySet()) {
            characters.add(characters.size() - entry.getKey() + 1, entry.getValue());
        }

        // RULE 6: return back capitalization
        for (int index : capitalization) {
            characters.set(index, characters.get(index).toUpperCase());
        }

        // concat chars back to word
        String finalResult = String.join("", characters);
        return finalResult;
    }

    /**
     * <p>Tests rule 3: word is ending with given ending.</p>
     *
     * @param givenWord checked word
     * @param ending    checked ending
     * @return if is word ending with given chars.
     */
    private static boolean isWordEndingWith(String givenWord, List<String> ending) {
        return givenWord.endsWith(String.join("", ending));
    }

    /**
     * <p>Returns all indexes of upper-case letters in word.</p>
     *
     * @param givenWord word which needs to be checked
     * @return list of indexes
     */
    private static List<Integer> getCapitalizationIndexes(String givenWord) {
        List<Integer> capitalization = new ArrayList<>();
        for (int index = 0; index < givenWord.length() - 1; index++) {
            // save capitalization indexes
            if (Character.isUpperCase(givenWord.charAt(index))) {
                capitalization.add(index);
            }
        }
        return capitalization;
    }

    /**
     * <p>Returns all punctuations and their positions in word (from start of the word).</p>
     *
     * @param characters all characters from the word
     * @return map containing punctuations with their indexes
     */
    private static Map<Integer, String> getPunctuations(List<String> characters) {
        Map<Integer, String> punctuations = new TreeMap<>();
        for (int index = 0; index < characters.size(); index++) {
            // save punctuation indexes
            if (PUNCTUATION.contains(characters.get(index))) {
                punctuations.put(characters.size() - index, characters.get(index));
            }
        }
        return punctuations;
    }

    /***
     * <p>Given word with hyphen splits in multiple words, processes them separately and returns them as one word.</p>
     * @param word  word with hyphen
     * @return processed word
     */
    private static String processHyphen(String word) {
        List<String> words = Arrays.asList(word.split("-"));
        String result = words.stream()
                .map(PigLatinUtil::processWord)
                .collect(Collectors.joining("-"));
        return result;
    }
}
