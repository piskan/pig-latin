package org.piglatin.translator;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.piglatin.core.utils.TestNGUtils.tc;
import static org.piglatin.core.utils.TestNGUtils.testCases;

/**
 * <p>Tests for {@link PigLatinUtil}</p>
 *
 * @author Petr Novák (petr.novak@starkysclub.com)
 */
public class PigLatinUtilTest {

    private static final String SENTENCE = "Is this the krusty krab? No, this is Patric.";
    private static final String EXPECTED_SENTENCE_RESULT = "Isway histay hetay rustykay rabkay? Onay, histay isway Atricpay.";

    /**
     * Tests the {@link PigLatinUtil#processWord(String)}
     */
    @Test(dataProvider = "testTranslateWordDataProvider")
    public void testTranslateWord(String input, String expectedOutput) {
        String output = PigLatinUtil.processWord(input);
        Assert.assertEquals(output, expectedOutput, "The output does not match expected result.");
    }

    @DataProvider(name = "testTranslateWordDataProvider")
    public Object[][] testTranslateWordDataProvider() {
        return testCases(
                tc("Hello", "Ellohay"),
                tc("Hello-world", "Ellohay-orldway"),
                tc("apple", "appleway"),
                tc("stairway", "stairway"),
                tc("can't", "antca'y"),
                tc("end.", "endway."),
                tc("Beach", "Eachbay"),
                tc("McCloud", "CcLoudmay"),
                tc("End.", "Endway."),
                tc("end.", "endway."),
                tc("Beach.", "Eachbay."),
                tc("McCloud", "CcLoudmay"),
                tc("Beac'h.", "Eachba'y."),
                tc("can't.", "antca'y."),
                tc("this-thing", "histay-hingtay")
        );
    }

    @Test(dataProvider = "testTranslateSentenceDataProvider")
    public void testTranslateSentence(String input, String expectedOutput) {
        String output = PigLatinUtil.processSentence(input);
        Assert.assertEquals(output, expectedOutput, "The output does not match expected result.");
    }

    @DataProvider(name = "testTranslateSentenceDataProvider")
    public static Object[][] testTranslateSentenceDataProvider() {
        return testCases(
                tc(SENTENCE, EXPECTED_SENTENCE_RESULT)
        );
    }
}