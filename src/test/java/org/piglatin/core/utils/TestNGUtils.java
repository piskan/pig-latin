package org.piglatin.core.utils;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * @author Michal Kourik (michalkourik@gmail.com) on 30.6.16
 * Utils for working with the framework TestNG
 */
public final class TestNGUtils {

    private TestNGUtils() {
    }

    /** Prepares the return object array for a method which accepts just one parameter
     * The input of this method is a static method which returns the parsed results
     * Throws {@link RuntimeException} when the given static provider provided no data */
    public static <E> Object[][] asSingleParameterDataProvider(Supplier<E[]> staticDataProvider) {
        E[] parameters = staticDataProvider.get();
        if (parameters.length == 0) {
            throw new RuntimeException("There was an error in the data provider - it did not provide any data.");
        }
        Object[][] testingData = new Object[parameters.length][1]; // we have testSearchModels.length instances of test data, but only 1 parameter to pass
        for (int i = 0; i < parameters.length; i++) {
            testingData[i][0] = parameters[i];
        }
        return testingData;
    }

    /** Prepares the return object array for a method which accepts one parameter and the second parameter is constant
     * The input of this method is a static method which returns the parsed results and the second static parameter
     * Throws {@link RuntimeException} when the given static provider provided no data */
    public static <E, F> Object[][] asSingleParameterDataProviderWithSecondConstantParameter(Supplier<E[]> staticDataProvider, F constantParameter) {
        E[] parameters = staticDataProvider.get();
        if (parameters.length == 0) {
            throw new RuntimeException("There was an error in the data provider - it did not provide any data.");
        }
        Object[][] testingData = new Object[parameters.length][2]; // we have testSearchModels.length instances of test data, but only 1 parameter to pass
        for (int i = 0; i < parameters.length; i++) {
            testingData[i][0] = parameters[i];
            testingData[i][1] = constantParameter;
        }
        return testingData;
    }

    /** Just a method for one test case in a data provider - provides just nicer syntax */
    public static Object[] tc(Object... data) {
        if (data == null) {
            return new Object[]{null};
        } else {
            return data;
        }
    }

    /** Just a method for one test case in a data provider - provides just nicer syntax */
    public static Object[] tcFunction(Supplier<Object[]> supplier) {
        return supplier.get();
    }

    /** Prepares a test cases from a collection supplier - creates a single test case for the each element in the collection */
    public static <E> Object[][] testCasesFromSupplier(Supplier<Collection<E>> function) {
        Collection<E> collection = function.get();
        Object[][] testCases = new Object[collection.size()][1];
        for (int i = 0; i < collection.size(); i++) {
            testCases[i][0] = collection.iterator().next();
        }
        return testCases;
    }

    /** Just provides us a better syntax in the data providers */
    public static Object[][] testCases(Object[]... data) {
        Object[][] result;
        if (data.length == 0) {
            result = new Object[][]{};
        } else {
            result = new Object[data.length][data[0].length];
        }
        for (int i = 0; i < data.length; i++) {
            Object[] testCaseData = data[i];
            for (int j = 0; j < testCaseData.length; j++) {
                result[i][j] = testCaseData[j];
            }
        }
        return result;
    }

    /** Just provides us a better syntax in the data providers */
    public static Object[][] testCasesWithId(Object[]... data) {
        Object[][] result;
        if (data.length == 0) {
            result = new Object[][]{};
        } else {
            result = new Object[data.length][data[0].length + 1];
        }
        for (int i = 0; i < data.length; i++) {
            Object[] testCaseData = data[i];
            for (int j = 0; j < testCaseData.length; j++) {
                result[i][j] = testCaseData[j];
            }
            result[i][data[0].length] = i;
        }
        return result;
    }
}
